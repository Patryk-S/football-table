<?php
    class Functions{
        private $_pointers = array();
        public function combinations(array $set, $subset_size = null){
            $set_size = count($set);
            if (is_null($subset_size)) {
                $subset_size = $set_size;
            }
            if ($subset_size >= $set_size) {
                return array($set);
            } else if ($subset_size == 1) {
                return array_chunk($set, 1);
            } else if ($subset_size == 0) {
                return array();
            }
            $combinations = array();
            $set_keys = array_keys($set);
            $this->_pointers = array_slice(array_keys($set_keys), 0, $subset_size);
            $combinations[] = $this->_getCombination($set);
            while ($this->_advancePointers($subset_size - 1, $set_size - 1)) {
                $combinations[] = $this->_getCombination($set);
            }
            return $combinations;
        }
        private function _advancePointers($pointer_number, $limit){
            if ($pointer_number < 0) {
                return false;
            }
            if ($this->_pointers[$pointer_number] < $limit) {
                $this->_pointers[$pointer_number]++;
                return true;
            } else {
                if ($this->_advancePointers($pointer_number - 1, $limit - 1)) {
                    $this->_pointers[$pointer_number] =
                        $this->_pointers[$pointer_number - 1] + 1;
                    return true;
                } else {
                    return false;
                }
            }
        }
        private function _getCombination($set){
            $set_keys = array_keys($set);
            $combination = array();
            foreach ($this->_pointers as $pointer) {
                $combination[$set_keys[$pointer]] = $set[$set_keys[$pointer]];
            }
            return $combination;
        }
        public function _array_flatten(array $array){
            $flat = array();
            $stack = array_values($array);
            while($stack){
                $value = array_shift($stack);
                if (is_array($value))
                    $stack = array_merge(array_values($value), $stack);
                else
                   $flat[] = $value;
            }
            return $flat;
        }
        public function combi_pairs(array $array){
            $merged = $this->_array_flatten($array);
            $arr = array();
            $unique_names = array();
            for($i=0, $j=0, $k=0;$i<=sizeof($merged);$i+=4, $j++){
                $arr[$j]=array($merged[$i+0],$merged[$i+1],$merged[$i+2],$merged[$i+3]);
                $arr[$j]=array_unique($arr[$j]);
                if(sizeof($arr[$j])===4) {
                    $unique_names[$k]=$arr[$j];
                    $k++;
                }
            }
            return $unique_names;
        }
    }
    // sekcja head
    echo '<html><head><title>Football table</title><link rel="stylesheet" href="style.css" type="text/css" media="screen" /></head><body>';
    // formularz ilosci osob
    echo '<form method="post"><div><input type="number" name="numberofplayers" value="4" /></div><input type="hidden" name="np" value="sent" /><input type="submit"  value="submit" /></form>';
    if (!empty($_POST["np"])) {
    // formularz nazw graczy
    echo '<form method="post">';
    for ($i = 0; $i < $_POST["numberofplayers"]; $i++)
        echo '<div><input name="players[]" type="text" /></div>';
        echo '<input type="hidden" name="op" value="sent" /><input type="submit"  value="submit" /></form>';
    }
    if (!empty($_POST["op"])) {
    //funkcje parujące
    $combinatorics = new Functions;
    $players=$_POST["players"];
    $combined_array = $combinatorics->combinations($players, 2); //mysqli_real_escape_string
    $combined_array2 = $combinatorics->combinations($combined_array, 2);
    
    $uniqueNames=$combinatorics->combi_pairs($combined_array2);
    //formularz punktacji
    echo "Drużyna A Drużyna B Wynik";
    echo '<form method="post">';
    for($i=0;$i<sizeof($uniqueNames);$i++){
        echo "<div>".$uniqueNames[$i][0]." i ".$uniqueNames[$i][1]." vs. ".$uniqueNames[$i][2]." i ".$uniqueNames[$i][3];
        echo "<input name='scoreA[]' type='number' />:<input name='scoreB[]' type='number' /></div>";
    }
    echo "<input type='hidden' name='players[]' value='".json_encode($players)."'>";
    echo "<input type='hidden' name='un[]' value='".json_encode($uniqueNames)."'>";
    echo '<input type="hidden" name="sp" value="sent" /><input type="submit" value="submit" /></form>';
    }
    if (!empty($_POST["sp"])) {
        $scoreA = $_POST["scoreA"];
        $scoreB = $_POST["scoreB"];
        $name =  $_POST["players"][0];// tablica - problem
        $uniqueNames = $_POST["un"][0];// tablica - problem
        for($i=0;$i<sizeof($un);$i++){
            if($scoreA[$i]>$scoreB[$i]){
                $score[$i][0]+=1;
                $score[$i][1]+=1;
            }
            else if($scoreA[$i]<$scoreB[$i]){
                $score[$i][2]+=1;
                $score[$i][3]+=1;
            }else if($scoreA[$i]===$scoreB[$i])
            echo "<div>Remis niemożliwy</div>";
        }
        echo json_encode($uniqueNames);
        echo json_encode($name);
        
        echo "<br>ScoreA:".json_encode($scoreA);
        echo "<br>ScoreB:".json_encode($scoreB);
        echo "<br>Score:".json_encode($score);
        // punktacja - problem
        for($i=0;$i<$name;$i++){
            for($j=0;$j<$uniqueNames;$j++){
                if($name[$i]==$uniqueNames[$j]){
                    $player[$i][0]=$name[$i];
                    $player[$i][1]+=score[$i];
                }
            }   
        }
        
        /*
        $servername = "localhost";
        $username = "xxx";
        $password = "xxx";
        $dbname = "xxx";
        
        $conn = new mysqli($servername, $username, $password, $dbname);
        
        if ($conn->connect_error)
            die("Connection failed: " . $conn->connect_error);
        
        for($i=0;$i<sizeof($name);$i++)
            $sql = "INSERT INTO `players` (names, scores) VALUES ($player[$i][0], $player[$i][1])";
        if ($conn->query($sql) === TRUE)
            echo "New record created successfully";
        else
            echo "Error: " . $sql . "<br>" . $conn->error;
        
        $sql = "SELECT (names, scores FROM players";
        $result = $conn->query($sql);

        if ($result->num_rows > 0)
            while($row = $result->fetch_assoc())
                echo "names: ".$row["names"]." - scores: ".$row["scores"]."<br>";
        else
            echo "0 results";
        
        $conn->close();
        */
    }
?>
</body>
</html>